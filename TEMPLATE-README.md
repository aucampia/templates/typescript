# typescript template

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,TEMPLATE-*.md,node_modules,coverage,dist*,mark,package-lock.json,yarn.lock} \
    ~/d/gitlab.com/aucampia/templates/typescript/ ./ --dry-run

## Diff summary ...
diff -u --exclude={.git,TEMPLATE-*.md,node_modules,coverage,dist*,mark,package-lock.json,yarn.lock} -r -q \
    ~/d/gitlab.com/aucampia/templates/typescript/ ./

## vimdiff
diff -u --exclude={.git,TEMPLATE-*.md,node_modules,coverage,dist*,mark,package-lock.json,yarn.lock} -r \
    ~/d/gitlab.com/aucampia/templates/typescript/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u --exclude={.git,TEMPLATE-*.md,node_modules,coverage,dist*,mark,package-lock.json,yarn.lock} -r \
    ~/d/gitlab.com/aucampia/templates/typescript/ ./
```

