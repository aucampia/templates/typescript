# typescript template

## TODO

 - [ ] ESLint: single file ...
 - [ ] ...

## ...

```bash
grep --exclude-dir={node_modules,dist-cjs} -ri tslint
```

```
npm install -g typescript
(cd /var/tmp/ && { \rm -v tsconfig.json || true; } && tsc --init) && vimdiff /var/tmp/tsconfig.json tsconfig.json
(cd /var/tmp/ && { \rm -v tsconfig.json || true; } && tsc --init) && vimdiff <(sed -E -e '/^\s*[/][/]/d' -e '/^\s*[/][*].*[*][/]\s*/d' -e '/^\s*$/d' -e 's,\s*/[*].*[*]/\s*$,,g' /var/tmp/tsconfig.json) tsconfig.json
```

```bash

find . \( -regex '^.*/.git$' \) -prune -o -type f -print \
    | xargs -n1 -t sed -E -n \
    -e 's|iwana([/.])template|xadix\1argparse_tree|gp' \
    -e 's|iwana|xadix|gp'

find . \( -regex '^.*/.git$' \) -prune -o -type f -print \
    | xargs -n1 -t sed -E -i \
    -e 's|iwana([/.])template|xadix\1argparse_tree|g' \
    -e 's|iwana|xadix|g'
```

## updating deps

```bash
npm update
yarn upgrade

## upgrade devDependencies
jq -r '.devDependencies | keys | .[]' package.json | xargs yarn add --dev
## upgrade dependencies
jq -r '.dependencies | keys | .[]' package.json | xargs yarn add

## upgrade devDependencies
jq -r '.devDependencies | keys | .[]' package.json | xargs npm install --save-dev
## upgrade dependencies
jq -r '.dependencies | keys | .[]' package.json | xargs npm install --save

npm install -g npm-check-updates
npm-check-updates -x '@types/node'
npm-check-updates -x '@types/node' -u
```

## node.js

* https://docs.npmjs.com/files/package.json
* https://docs.npmjs.com/misc/scripts
* https://docs.npmjs.com/cli/run-script
* https://docs.npmjs.com/cli/update
* https://docs.npmjs.com/cli/outdated
* https://docs.npmjs.com/cli/dist-tag
* https://docs.npmjs.com/cli/version
* https://docs.npmjs.com/cli/publish

* https://semver.npmjs.com/
* https://www.npmjs.com/package/semver
* https://docs.npmjs.com/misc/semver

## Tools

### tslint

* https://palantir.github.io/tslint/
* https://www.npmjs.com/package/tslint
* https://github.com/palantir/tslint

### eslint

* https://eslint.org/
* https://www.npmjs.com/package/eslint
* https://github.com/eslint/eslint

### typedoc

* https://typedoc.org/
* https://www.npmjs.com/package/typedoc
* https://github.com/TypeStrong/TypeDoc

## jsdoc

* https://devdocs.io/jsdoc/
* https://www.npmjs.com/package/jsdoc
* https://github.com/jsdoc/jsdoc

## Tools

setup node.js

```bash
yum install https://rpm.nodesource.com/pub_10.x/el/7/x86_64/nodesource-release-el7-1.noarch.rpm
dnf install https://rpm.nodesource.com/pub_10.x/fc/29/x86_64/nodesource-release-fc29-1.noarch.rpm
```

In `~/.bash_profile`

```bash
## nodejs:BEGIN
export NPM_PACKAGES="${HOME}/.npm-packages"
export PATH="${NPM_PACKAGES}/bin${PATH:+:${PATH}}"
export NODE_PATH="${NPM_PACKAGES}/lib/node_modules${NODE_PATH:+:${NODE_PATH}}"
export MANPATH="$NPM_PACKAGES/share/man:${MANPATH:-$(manpath)}"
## nodejs:END
```

In `~/.npmrc`

```conf
prefix=~/.npm-packages
```

```bash
npm install -g typescript ts-node

npm --init
tsc --init
```

### nodenv

```bash
git clone https://github.com/nodenv/nodenv.git ~/.nodenv
git clone https://github.com/nodenv/node-build.git ~/.nodenv/plugins/node-build
```

```
export PATH="$HOME/.nodenv/bin:$PATH"
eval "$(nodenv init -)"
```

```
exec bash -l
nodenv install ...
nodenv global ...
which node
node --version
```

## other

```
npm run xscripts -- -v -v -v run ftsf
npm run xscripts -- -v -v -v run build

npm install -g npm-check-updates

ncu
ncu --upgrade
ncu --upgrade --interactive
ncu --upgrade --semverLevel minor --loglevel verbose
ncu --upgrade --semverLevel major --loglevel verbose
```

```bash
npm login --registry=https://registry.npmjs.org/ --scope=@iwan.aucamp --always-auth
```

```bash
semver -i prerelease --preid alpha "$(jq -r .version package.json)"
npm version --preid=alpha prerelease && npm publish

semver -i prerelease --preid beta "$(jq -r .version package.json)"
npm version --preid=beta prerelease && npm publish
```

```bash
semver -i prerelease --preid rc "$(jq -r .version package.json)"
npm version --preid=rc prerelease && npm publish

semver -i preminor --preid beta "$(jq -r .version package.json)"
npm version --preid=beta preminor && npm publish
```

```bash
echo "$(semver -i minor "$(jq -r .version package.json)")-SNAPSHOT.$(TZ=Etc/UTC date +%Y%m%d%H%M%S)"
npm --no-git-tag-version version "$(semver -i minor "$(jq -r .version package.json)")-SNAPSHOT.$(TZ=Etc/UTC date +%Y%m%d%H%M%S)"
```

```bash
npm run test -- '__tests__/sample'
```

## templates

 - https://github.com/microsoft/TypeScript-Node-Starter
 - https://flaviocopes.com/package-json/
 - https://corgibytes.com/blog/2017/04/18/npm-tips/
 - https://old.reddit.com/r/node/comments/6ybce0/do_you_use_colons_or_dashes_to_prefix_npm_scripts/
 - https://www.npmjs.com/package/npm-run-all

## licenses

* https://spdx.org/licenses/0BSD.html
* https://opensource.org/licenses/0BSD
* https://choosealicense.com/licenses/0bsd/
* https://en.wikipedia.org/wiki/BSD_licenses#0-clause_license_(%22Zero_Clause_BSD%22)
* https://tldrlegal.com/license/bsd-0-clause-license
* https://choosealicense.com/appendix/
* https://en.wikipedia.org/wiki/Public-domain-equivalent_license
* https://landley.net/toybox/license.html
* https://github.com/landley/toybox/blob/master/LICENSE

## guidelines

* https://www.gnu.org/prep/standards/standards.html
* https://www.gnu.org/prep/standards/standards.html#Directory-Variables
* https://www.gnu.org/prep/standards/standards.html#Standard-Targets

```bash
vimdiff <(curl --silent https://raw.githubusercontent.com/landley/toybox/master/LICENSE) LICENSE.txt
```


## refs

 - https://github.com/yargs/yargs/blob/master/docs/advanced.md
 - https://github.com/yargs/yargs/blob/master/docs/api.md
 - https://github.com/yargs/yargs/blob/master/docs/tricks.md
 - https://github.com/yargs/yargs/blob/master/docs/examples.md
